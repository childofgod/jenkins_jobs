#!/bin/bash
set -o pipefail # trace ERR through pipes                                          
set -o errtrace # trace ERR through 'time command' and other functions             
set -o nounset ## set -u : exit the script if you try to use an uninitialised variable
set -e ## exit when an error occurs (and set build to failed)

mountpoint /media/BACKUP || sudo mount /media/BACKUP
sleep 1
mountpoint /media/BACKUP || exit 100

mountpoint /media/BILDER || sudo mount /media/BILDER
sleep 1
mountpoint /media/BILDER || exit 200

sudo rsync -avrR --progress --delete --backup --backup-dir=/media/BACKUP/backup_papierkorb/josua /media/BILDER /media/BACKUP/Backup/josua/
