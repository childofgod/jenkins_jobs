#!/bin/bash
set -o pipefail # trace ERR through pipes                                          
set -o errtrace # trace ERR through 'time command' and other functions             
set -o nounset ## set -u : exit the script if you try to use an uninitialised variable
set -e ## exit when an error occurs (and set build to failed)

mountpoint /media/BACKUP || sudo mount /media/BACKUP
sleep 1
mountpoint /media/BACKUP || exit 100

sudo rsync --backup --delete --exclude=.mozilla --exclude=.cache --exclude=.thumbnails --exclude=.local/share/Trash --exclude=.lastpass -avRr /home/ /media/BACKUP/Backup/josua/home/
