#!/bin/bash
set -o pipefail # trace ERR through pipes                                          
set -o errtrace # trace ERR through 'time command' and other functions             
set -o nounset ## set -u : exit the script if you try to use an uninitialised variable
set -e ## exit when an error occurs (and set build to failed)


if lsof /VMSYS/Avid_VM_erstellt/XP_Avid_2.8 ; then
    echo VM läuft
    exit 1
else
    sudo rsync --delete -avRr /VMSYS/Avid_VM_erstellt /media/USB3/josua/
fi

if lsof /VIDEO/AvidMediaFiles_Finn ; then
    echo VM läuft
    exit 1
else
    sudo rsync --delete -avRr /VMSYS/Avid_VM_erstellt /media/USB3/josua/
fi
