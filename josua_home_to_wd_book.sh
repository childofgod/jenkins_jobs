#!/bin/bash
set -o pipefail # trace ERR through pipes                                          
set -o errtrace # trace ERR through 'time command' and other functions             
set -o nounset ## set -u : exit the script if you try to use an uninitialised variable
set -e ## exit when an error occurs (and set build to failed)

mountpoint /media/USB3 || sudo mount /media/USB3 
mountpoint /media/USB3 || exit 1
sudo rsync --backup --delete --exclude=.mozilla --exclude=.cache --exclude=.thumbnails --exclude=.local/share/Trash --exclude=.lastpass -avRr /home/ /media/USB3/josua/
